// in: Chen Lin Yu 123
// out: 123 Chen Lin Yu

#include <iostream>
#include <string.h>
using namespace std;

#define BUFFER 50

void exchange(char **pt_name, char **pt_number)
{
    char *pt_temp = NULL;
    pt_temp = *pt_name;
    *pt_name = *pt_number;
    *pt_number = pt_temp;
}

int main()
{
    char name[20] = {0};
    char number[20] = {0};
    char input_str[BUFFER] = {0};
    char *pt_name = name;
    char *pt_number = number;
    int len = 0;
    int idx_name = 0;
    int idx_number = 0;
    int seg_index = 0;

    cout << "please input name and number, then press enter" << endl;

    cin.get(input_str, BUFFER);
    cout << "input: " << input_str << endl;

    len = strlen(input_str);
    for (int i = len - 1; i >= 0; i--) {
         if (input_str[i] == ' ') {
            seg_index = i;
            break;
         }
    }
    for (int i = 0; i < seg_index; i++) {
        name[idx_name] = input_str[i];
        idx_name ++;
    }
    for (int i = seg_index + 1; i < len; i++) {
        number[idx_number] = input_str[i];
        idx_number ++;
    }

    cout << "before exchange: " << pt_name << " " <<pt_number << endl;

    // way1: direct exchange
    // char *pt_temp = NULL;
    // pt_temp = name;
    // pt_name = pt_number;
    // pt_number = pt_temp;

    // way2: function exchange
    exchange(&pt_name, &pt_number);

    cout << "after  exchange: "<< pt_name << " " <<pt_number << endl;
    return 0;
}
