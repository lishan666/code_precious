#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
r_dict={}
dict={}
r_list=[]
wrongRegex=re.compile("[^a-zA-Z0-9]")
while True:
    print("***********************")
    print("【欢迎使用】")
    print("1.注册用户")
    print("2.用户登录")
    print("3.退出系统")
    print("***********************")
    a=input("请选择：")
    if a in ["1","2","3"]:
        if a=="1":
            print("*************")
            print("注册用户")
            print("*************")
            while True:
                name = input("请输入用户名【提示:只能为字母下划线和数字】:")
                if wrongRegex.search(name) != None:
                   print("包含无效字符！")
                else:
                    break
            if name not in dict:
                while True:
                    while True:
                        secret=input("请输入密码【提示:小于八位或包含大小写字母和数字的密码】:")
                        if len(secret)>8:
                            print("你输入的密码长度不符合要求，请重新输入！")
                        elif wrongRegex.search(secret) !=None:
                            print("包含无效字符！")
                        else:
                            break
                    secret1=input("请再次输入密码:")
                    if secret==secret1:
                        r_dict={name:secret}
                        print(r_dict)
                        dict.update(r_dict)
                        print(dict)
                        with open("user.txt","a") as f:
                            f.write(name)
                            f.write(":")
                            f.write(secret   )
                            f.write("   ")
                        break
                    else:
                        print("您输入的密码不一致，请重新输入")
            else:
                print("该用户名已经存在，请勿重复注册！")
        elif a=="2":
            j=0
            print("*************")
            print("用户登录")
            print("*************")
            filename=input("用户名:")
            if filename in r_list:
                print(filename)
                print("黑名单用户，禁止登录")
                continue
            if filename in dict:
                    while j<3:
                        filesecret=input("密码:")
                        if filesecret==dict[filename]:
                            print("登陆成功！")
                            b= int(input("请选择:\n""1.查看信息\n""2.修改密码\n""3.设置\n""4.返回上一层\n"))
                            print("您执行的操作是%d:"%b)
                            if b==1:
                                print("查看信息")
                            elif b==2:
                                print("修改密码")
                            elif b==3:
                                print("设置")
                            elif b==4:
                                print("返回上一层")

                        else:
                            print("您输入的密码有误，请重新输入！")
                        j+=1
                    r_list.append(filename)
                    with open("blacklist.txt", "a") as t:
                        t.write(filename)
            else:
                    print("该用户名不存在，请注册！！")
        elif a=="3":
           exit()
    else:
        print("错误操作！")
