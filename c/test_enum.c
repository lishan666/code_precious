#include <stdio.h>

int func(void)
{
	printf("func\n");
	return 0;
}

int main(void)
{
	typedef enum {
		one = -1,
		two = 2,
	}week_test;
	week_test week;
	week = one;
	register int a = 0;
	printf("one = %d, a = %d\n", week, a);
	printf("%s->\n", __FILE__);
	func();
	return 0;
}
