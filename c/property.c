#include<stdlib.h>
#include <string.h>
#include <stdio.h>

typedef enum {
    MESON_DRM_DISCONNECTED      = 0,
    MESON_DRM_CONNECTED         = 1,
    MESON_DRM_UNKNOWNCONNECTION = 2
} ENUM_MESON_DRM_CONNECTION;

ENUM_MESON_DRM_CONNECTION meson_drm_getConnection()
{
    ENUM_MESON_DRM_CONNECTION ret = MESON_DRM_UNKNOWNCONNECTION;
    int ConnectState = 2;

    if (ConnectState == 1)
        ret = MESON_DRM_CONNECTED;
    else if (ConnectState == 2)
        ret = MESON_DRM_DISCONNECTED;
    else
        ret = MESON_DRM_UNKNOWNCONNECTION;
    return ret;
}


int main ()
{
    ENUM_MESON_DRM_CONNECTION  enumCon = meson_drm_getConnection();
    int connected = 0;

    if(enumCon == 1)
        connected = 1;
    else
        connected = 0;
    printf("result: %d\n",connected);
    return 0;
}

