#include <stdio.h>

enum Day {
    WON = -1,
    TUE,
};

int main(void)
{
    enum Day day = WON;
    printf("day: %d\n", day);
    day = TUE;
    printf("day: %d\n", day);
    return 0;
}
